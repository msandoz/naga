package org.sandcast.naga;

public class TWSTick {

    private String tick;
    private int field;
    private double price;
    private int canAutoExecute;
    private static final String[] STOCKS = new String[]{"MSFT", "IBM", "AAPL", "CSCO"};

    public static final TWSTick random() {
        String tick = randomStock();
        int field = 3;
        double price = Math.random() * 100.0;
        return new TWSTick(tick, field, price, 0);
    }

    public static final String randomStock() {
        return STOCKS[Math.round((float) (Math.random() * 3.0))];
    }

    public TWSTick(String tick, int field, double price, int canAutoExecute) {
        this.tick = tick;
        this.field = field;
        this.price = price;
        this.canAutoExecute = canAutoExecute;
    }

    public String getTickerId() {
        return tick;
    }

    public void setTickerId(String ticker) {
        this.tick = ticker;
    }

    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCanAutoExecute() {
        return canAutoExecute;
    }

    public void setCanAutoExecute(int canAutoExecute) {
        this.canAutoExecute = canAutoExecute;
    }

}
