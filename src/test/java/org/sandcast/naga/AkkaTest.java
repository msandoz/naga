package org.sandcast.naga;

import akka.actor.ActorRef;
import org.sandcast.naga.actor.StockExchangeActor;
import akka.actor.ActorSystem;
import akka.actor.Props;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.Test;
import org.sandcast.naga.message.distributed.migration.MigrateActor;

public class AkkaTest {

    @Test
    public void tryIt() {
        final ActorSystem system = ActorSystem.create("mustream");
        ActorRef actor = system.actorOf(Props.create(StockExchangeActor.class, new Object[]{}), "stockexchange");
        try {
            Thread.sleep(1_000L);
            MigrateActor ma = new MigrateActor("IOT-2", actor.path().toStringWithoutAddress() + "/exchange/agency/strategy-MSFT", "");
            system.actorSelection(actor.path().toStringWithoutAddress() + "/distributed/rabbitmq1").tell(ma, actor);
            Thread.sleep(3_000L);

        } catch (InterruptedException ex) {
        }
    }

}
