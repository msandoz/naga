package org.sandcast.naga.actor;

import org.sandcast.naga.dto.PersistedActorDTO;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import org.sandcast.naga.dto.BehaviorDTO;
import org.sandcast.naga.message.distributed.container.AddTransport;

public class StockExchangeActor
        extends AbstractActor {

    private static final String GROOVY_TICK_SCRIPT = ""
            + "import org.sandcast.naga.message.*;"
            + "import org.sandcast.naga.message.management.*;"
            + "import org.sandcast.naga.*;"
            + "String stock = TWSTick.randomStock();"
            + "self.tell(new MessageForward(new MuStreamMessageImpl(\"3.19\", \"tick\", stock)), self);"
            + "return \"Ticked \" + stock +\"@3.19\"";
    private final LoggingAdapter log = Logging.getLogger(context().system(), this);

    public StockExchangeActor() {
        ActorRef transport = context().actorOf(Props.create(TransportManagerActor.class, "IOT-1"), "distributed");
        ActorRef transport2 = context().actorOf(Props.create(TransportManagerActor.class, "IOT-2"), "distributed2");
        AddTransport att = new AddTransport(Props.create(RabbitMQTransportActor.class, "IOT-1"), "rabbitmq1");
        AddTransport att2 = new AddTransport(Props.create(RabbitMQTransportActor.class, "IOT-2"), "rabbitmq2");
        transport.tell(att, self());
        transport2.tell(att2, self());
        PersistedActorDTO tickera = new PersistedActorDTO();
        tickera.setName("ticker");
        tickera.setBehavior(new BehaviorDTO("org.sandcast.naga.behavior.GroovyBehavior", new String[]{"", GROOVY_TICK_SCRIPT}));
        tickera.setIsCyclic(true);
        tickera.setDuration(1L);
        tickera.setDurationUnit("SECONDS");
        tickera.getForwards().add("../agency/strategy-*");

        PersistedActorDTO bookkeeper = new PersistedActorDTO();
        bookkeeper.setName("bookkeeper");
        bookkeeper.setBehavior(new BehaviorDTO("org.sandcast.naga.behavior.BookkeeperBehavior", new String[]{"", ""}));

        PersistedActorDTO ordera = newGroovyActor("order", "", "../../bookkeeper", "x=\"ORDERED/\"");
        PersistedActorDTO protectora = newGroovyActor("protector-MSFT", "MSFT", "../order", "x=\"PROTECTED/\"");
        PersistedActorDTO strategya = newGroovyActor("strategy-MSFT", "MSFT", "../protector-MSFT", "x=\"STRATEGIZED/\"");
        PersistedActorDTO protectorb = newGroovyActor("protector-IBM", "IBM", "../order", "x=\"PROTECTED/\"");
        PersistedActorDTO strategyb = newGroovyActor("strategy-IBM", "IBM", "../protector-IBM", "x=\"STRATEGIZED/\"");

        PersistedActorDTO agencya = new PersistedActorDTO();
        agencya.setName("agency");
        agencya.getChildren().add(ordera);
        agencya.getChildren().add(protectora);
        agencya.getChildren().add(strategya);
        agencya.getChildren().add(protectorb);
        agencya.getChildren().add(strategyb);

        PersistedActorDTO exchange = new PersistedActorDTO();
        exchange.setName("exchange");
        exchange.getChildren().add(agencya);
        exchange.getChildren().add(tickera);
        exchange.getChildren().add(bookkeeper);
        context().actorOf(Props.create(MuStreamActor.class, exchange), "exchange");

        ObjectMapper mapper = new ObjectMapper();

        try {
//            mapper.writerWithDefaultPrettyPrinter().writeValue(System.out, exchange);
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File("target/classes/exchange.json"), exchange);
        } catch (IOException ex) {
            log.error(ex, "error writing json out");
        }
        try {
            mapper.readValue(new File("target/classes/exchange.json"), PersistedActorDTO.class);
//            mapper.writerWithDefaultPrettyPrinter().writeValue(System.out, exch);
        } catch (IOException ex) {
            log.error(ex, "error reading json in");
        }
        this.receive(ReceiveBuilder.matchAny(s -> {
        }).build());
    }

    private PersistedActorDTO newGroovyActor(String name, String routingKey, String forward, String text) {
        PersistedActorDTO ordera = new PersistedActorDTO();
        ordera.setName(name);
        ordera.setBehavior(new BehaviorDTO("org.sandcast.naga.behavior.GroovyBehavior", new String[]{routingKey, text}));
        ordera.getForwards().add(forward);
        return ordera;
    }
}
