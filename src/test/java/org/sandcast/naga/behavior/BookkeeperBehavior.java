package org.sandcast.naga.behavior;

import akka.actor.ActorContext;
import org.sandcast.naga.message.MuStreamMessageImpl;

public class BookkeeperBehavior implements MessageConsumingBehavior {

    public BookkeeperBehavior(String routingKey, String other) {
        //think this through better. will have to base on a props probably?
    }

    @Override
    public String apply(MuStreamMessageImpl msg, ActorContext context) {
        context.system().log().warning("Kept up the books with my non-groovy custom code for " + msg.getContents());
        return "";
    }

    @Override
    public boolean test(MuStreamMessageImpl t) {
        return true;
    }

    @Override
    public String toString() {
        return "bookkeeper actor";
    }

}
