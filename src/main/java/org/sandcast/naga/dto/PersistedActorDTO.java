package org.sandcast.naga.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

public class PersistedActorDTO {

    private String name;
    private boolean isCyclic;
    private Long duration;
    private String durationUnit;
    private BehaviorDTO behavior;
    private List<String> forwards;
    private List<PersistedActorDTO> children;

    public PersistedActorDTO() {
        forwards = new ArrayList<>();
        children = new ArrayList<>();
    }

    public List<PersistedActorDTO> getChildren() {
        return children;
    }

    public String getName() {
        return name;
    }

    public boolean isIsCyclic() {
        return isCyclic;
    }

    public Long getDuration() {
        return duration;
    }

    public BehaviorDTO getBehavior() {
        return behavior;
    }

    public List<String> getForwards() {
        return forwards;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIsCyclic(boolean isCyclic) {
        this.isCyclic = isCyclic;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public void setBehavior(BehaviorDTO behavior) {
        this.behavior = behavior;
    }

    public void setForwards(List<String> forwards) {
        this.forwards = forwards;
    }

    public void setChildren(List<PersistedActorDTO> children) {
        this.children = children;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

}
