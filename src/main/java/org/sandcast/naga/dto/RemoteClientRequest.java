package org.sandcast.naga.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RemoteClientRequest {

    private final String container;
    private final String transportPath;
    private final String requester;
    private final Object message;

    public @JsonCreator
    RemoteClientRequest(
            @JsonProperty("container") String container, 
            @JsonProperty("transportPath") String transportPath,
            @JsonProperty("requester") String requester,
            @JsonProperty("message") Object message) {
        this.container = container;
        this.transportPath = transportPath;
        this.requester = requester;
        this.message = message;
    }

    public String getContainer() {
        return container;
    }

    public String getTransportPath() {
        return transportPath;
    }

    public String getRequester() {
        return requester;
    }

    public Object getMessage() {
        return message;
    }

}
