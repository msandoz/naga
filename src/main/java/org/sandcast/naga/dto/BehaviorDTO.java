package org.sandcast.naga.dto;

public class BehaviorDTO {
private String className;
private String[] constructorArgs;

    public BehaviorDTO() {
    }

    public BehaviorDTO(String className, String... constructorArgs) {
        this.className = className;
        this.constructorArgs = constructorArgs;
    }

    public void setConstructorArgs(String... constructorArgs) {
        this.constructorArgs = constructorArgs;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public String[] getConstructorArgs() {
        return constructorArgs.clone();
    }

}
