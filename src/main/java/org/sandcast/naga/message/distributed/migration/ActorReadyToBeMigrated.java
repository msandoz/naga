package org.sandcast.naga.message.distributed.migration;

import org.sandcast.naga.dto.PersistedActorDTO;
import org.sandcast.naga.message.distributed.RemotelyAddressed;
import org.sandcast.naga.message.distributed.Transportable;

public class ActorReadyToBeMigrated implements Transportable, RemotelyAddressed {

    private final PersistedActorDTO state;
    private final String transport;
    private final String remoteAddress;
    private final String originalActorPath;

    public ActorReadyToBeMigrated(PersistedActorDTO state, String transport, String remoteAddress, String originalActorPath) {
        this.state = state;
        this.transport = transport;
        this.remoteAddress = remoteAddress;
        this.originalActorPath = originalActorPath;
    }

    public PersistedActorDTO getState() {
        return state;
    }

    public String getTransport() {
        return transport;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public String getOriginalActorPath() {
        return originalActorPath;
    }

}
