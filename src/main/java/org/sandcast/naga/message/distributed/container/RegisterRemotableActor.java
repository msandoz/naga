package org.sandcast.naga.message.distributed.container;

public class RegisterRemotableActor {

    private final String actorPath;

    public RegisterRemotableActor(String actorPath) {
        this.actorPath = actorPath;
    }

    public String getActorPath() {
        return actorPath;
    }

}
