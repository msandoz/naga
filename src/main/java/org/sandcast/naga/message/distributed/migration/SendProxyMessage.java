package org.sandcast.naga.message.distributed.migration;

import org.sandcast.naga.message.MuStreamMessageImpl;
import org.sandcast.naga.message.distributed.RemotelyAddressed;

public class SendProxyMessage implements RemotelyAddressed {

    private final MuStreamMessageImpl message;
    private final String remoteAddress;
    private final String targetActorPath;

    public SendProxyMessage(MuStreamMessageImpl message, String remoteAddress, String targetActorPath) {
        this.message = message;
        this.remoteAddress = remoteAddress;
        this.targetActorPath = targetActorPath;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public MuStreamMessageImpl getMessage() {
        return message;
    }

    public String getTargetActorPath() {
        return targetActorPath;
    }

}
