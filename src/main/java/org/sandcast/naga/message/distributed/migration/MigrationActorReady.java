package org.sandcast.naga.message.distributed.migration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.sandcast.naga.message.distributed.RemotelyAddressed;

public class MigrationActorReady implements RemotelyAddressed {

    private final String createdPath;
    private final String remoteAddress;
    private final String originalActorPath;
    @JsonCreator

    public MigrationActorReady(@JsonProperty("createdPath") String createdPath, @JsonProperty("remoteAddress")String remoteAddress, @JsonProperty("originalActorPath")String originalActorPath) {
        this.createdPath = createdPath;
        this.remoteAddress = remoteAddress;
        this.originalActorPath = originalActorPath;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public String getCreatedPath() {
        return createdPath;
    }

    public String getOriginalActorPath() {
        return originalActorPath;
    }
}
