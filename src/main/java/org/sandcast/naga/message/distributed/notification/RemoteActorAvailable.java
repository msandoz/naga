package org.sandcast.naga.message.distributed.notification;

public class RemoteActorAvailable {

    private final String container;
    private final String actorPath;

    public RemoteActorAvailable(String container, String actorPath) {
        this.container = container;
        this.actorPath = actorPath;
    }

    public String getContainer() {
        return container;
    }

    public String getActorPath() {
        return actorPath;
    }

}
