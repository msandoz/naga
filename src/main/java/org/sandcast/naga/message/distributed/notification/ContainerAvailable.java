package org.sandcast.naga.message.distributed.notification;

public class ContainerAvailable {

    private final String container;
    private final String transport;

    public ContainerAvailable(String container, String transport) {
        this.container = container;
        this.transport = transport;
    }

    public String getContainer() {
        return container;
    }

    public String getTransport() {
        return transport;
    }

}
