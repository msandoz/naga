package org.sandcast.naga.message.distributed.container;

import akka.actor.Props;

public class CreateRemotableActor {

    private final String name;
    private final Props properties;

    public CreateRemotableActor(String name, Props properties) {
        this.name = name;
        this.properties = properties;
    }

    public String getName() {
        return name;
    }

    public Props getProperties() {
        return properties;
    }

}
