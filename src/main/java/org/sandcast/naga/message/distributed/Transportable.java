package org.sandcast.naga.message.distributed;

public interface Transportable {

    public String getTransport();
}
