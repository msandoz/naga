package org.sandcast.naga.message.distributed.notification;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContainerHeartbeat {

    private final String container;
    @JsonCreator
    public ContainerHeartbeat(@JsonProperty("container") String container) {
        this.container = container;
    }

    public String getContainer() {
        return container;
    }

    @Override
    public String toString() {
        return "CONTAINER " + container + " IS UP!!!!";
    }

}
