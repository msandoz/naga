package org.sandcast.naga.message.distributed.migration;

import org.sandcast.naga.message.distributed.Transportable;
import org.sandcast.naga.message.distributed.RemotelyAddressed;

public class MigrateActor implements Transportable, RemotelyAddressed {

    private final String remoteAddress;
    private final String actorPath;
    private final String transport;

    public MigrateActor(String remoteAddress, String actorPath, String transport) {
        this.remoteAddress = remoteAddress;
        this.actorPath = actorPath;
        this.transport = transport;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public String getTransport() {
        return transport;
    }

    public String getActorPath() {
        return actorPath;
    }

}
