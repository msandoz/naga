package org.sandcast.naga.message.distributed.migration;

import org.sandcast.naga.message.distributed.Transportable;
import org.sandcast.naga.message.distributed.RemotelyAddressed;

public class PrepareForMigration implements Transportable, RemotelyAddressed {

    private final String remoteAddress;
    private final String transport;

    public PrepareForMigration(String remoteAddress, String transport) {
        this.remoteAddress = remoteAddress;
        this.transport = transport;
    }

    public String getTransport() {
        return transport;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

}
