package org.sandcast.naga.message.distributed.container;

import akka.actor.Props;

public class AddTransport {

    private final Props props;
    private final String alias;

    public AddTransport(Props props, String alias) {
        this.props = props;
        this.alias = alias;
    }

    public Props getProps() {
        return props;
    }

    public String getAlias() {
        return alias;
    }

}
