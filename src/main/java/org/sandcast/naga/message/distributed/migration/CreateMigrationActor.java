package org.sandcast.naga.message.distributed.migration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.sandcast.naga.dto.PersistedActorDTO;
import org.sandcast.naga.message.distributed.RemotelyAddressed;

public class CreateMigrationActor implements RemotelyAddressed {

    private final PersistedActorDTO state;
    private final String remoteAddress;
    private final String originalActorPath;

    @JsonCreator
    public CreateMigrationActor(@JsonProperty("state") PersistedActorDTO state, @JsonProperty("remoteAddress") String remoteAddress, @JsonProperty("originalActorPath") String originalActorPath) {
        this.state = state;
        this.remoteAddress = remoteAddress;
        this.originalActorPath = originalActorPath;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public PersistedActorDTO getState() {
        return state;
    }

    public String getOriginalActorPath() {
        return originalActorPath;
    }

}
