package org.sandcast.naga.message.distributed.migration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.sandcast.naga.message.MuStreamMessageImpl;

public class ProxyMessage {

    private final MuStreamMessageImpl message;
    private final String targetActorPath;

    @JsonCreator
    public ProxyMessage(@JsonProperty("message") MuStreamMessageImpl message, @JsonProperty("targetActorPath") String targetActorPath) {
        this.message = message;
        this.targetActorPath = targetActorPath;
    }

    public MuStreamMessageImpl getMessage() {
        return message;
    }

    public String getTargetActorPath() {
        return targetActorPath;
    }

}
