package org.sandcast.naga.message.distributed.migration;

import org.sandcast.naga.message.distributed.RemotelyAddressed;

public class SendMigrationActorReady implements RemotelyAddressed {

    private final String createdPath;
    private final String remoteAddress;
    private final String originalActorPath;

    public SendMigrationActorReady(String createdPath, String remoteAddress, String originalActorPath) {
        this.createdPath = createdPath;
        this.remoteAddress = remoteAddress;
        this.originalActorPath = originalActorPath;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public String getCreatedPath() {
        return createdPath;
    }

    public String getOriginalActorPath() {
        return originalActorPath;
    }
}
