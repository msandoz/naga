package org.sandcast.naga.message.distributed;

public interface RemotelyAddressed {

    public String getRemoteAddress();
}
