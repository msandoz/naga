package org.sandcast.naga.message.distributed.notification;

public class RemoteContainerAvailable {

    private final String path;
    private final String alias;

    public RemoteContainerAvailable(String path, String alias) {
        this.path = path;
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }

    public String getPath() {
        return path;
    }

}
