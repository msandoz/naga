package org.sandcast.naga.message.distributed.actor;

import org.sandcast.naga.message.distributed.RemotelyAddressed;
import org.sandcast.naga.message.distributed.Transportable;

public class BecomeProxy implements Transportable, RemotelyAddressed {

    private final String destinationPath;
    private final String remoteAddress;
    private final String transport;

    public BecomeProxy(String transport, String remoteAddress, String destinationPath) {
        this.destinationPath = destinationPath;
        this.remoteAddress = remoteAddress;
        this.transport = transport;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public String getTransport() {
        return transport;
    }
}
