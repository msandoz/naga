package org.sandcast.naga.message.distributed.migration;

import org.sandcast.naga.dto.PersistedActorDTO;
import org.sandcast.naga.message.distributed.RemotelyAddressed;

public class SendCreateMigrationActor implements RemotelyAddressed {

    private final PersistedActorDTO state;
    private final String remoteAddress;
    private final String originalActorPath;

    public SendCreateMigrationActor(PersistedActorDTO state, String remoteAddress, String originalActorPath) {
        this.state = state;
        this.remoteAddress = remoteAddress;
        this.originalActorPath = originalActorPath;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public PersistedActorDTO getState() {
        return state;
    }

    public String getOriginalActorPath() {
        return originalActorPath;
    }

}
