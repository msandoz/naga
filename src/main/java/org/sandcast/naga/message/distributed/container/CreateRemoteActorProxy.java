package org.sandcast.naga.message.distributed.container;

public class CreateRemoteActorProxy {

    private final String remotePath;
    private final String localAlias;

    public CreateRemoteActorProxy(String remotePath, String localAlias) {
        this.remotePath = remotePath;
        this.localAlias = localAlias;
    }

    public String getLocalAlias() {
        return localAlias;
    }

    public String getRemotePath() {
        return remotePath;
    }
    
}
