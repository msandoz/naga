package org.sandcast.naga.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MuStreamMessageImpl {

    private final String routingKey;
    private final String messageType;
    private final String contents;

    @JsonCreator
    public MuStreamMessageImpl(@JsonProperty("routingKey") String routingKey, @JsonProperty("messageType") String messageType, @JsonProperty("contents") String contents) {
        this.routingKey = routingKey;
        this.messageType = messageType;
        this.contents = contents;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getContents() {
        return contents;
    }

    @Override
    public String toString() {
        return getMessageType() + "::" + getRoutingKey() + "::" + getContents();
    }
}
