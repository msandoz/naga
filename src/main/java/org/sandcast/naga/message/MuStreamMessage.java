package org.sandcast.naga.message;

public interface MuStreamMessage<T> {

    String getRoutingKey();
    String getMessageType();
    T getMessageContents();
}
