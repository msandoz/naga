package org.sandcast.naga.message.management;

import scala.concurrent.duration.FiniteDuration;

public class CycleSetState {

    private final boolean cyclic;
    private final FiniteDuration duration;

    public CycleSetState(boolean cyclic, FiniteDuration duration) {
        this.cyclic = cyclic;
        this.duration = duration;
    }

    public boolean isCyclic() {
        return cyclic;
    }

    public FiniteDuration getDuration() {
        return duration;
    }

}
