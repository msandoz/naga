package org.sandcast.naga.message.management;

public class ChildRemove {

    private final String name;

    public ChildRemove(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
