package org.sandcast.naga.message.management;

import org.sandcast.naga.message.MuStreamMessageImpl;

public class CycleStart extends MuStreamMessageImpl {

    private final Long cycleId;

    public CycleStart(Long cycleId) {
        super(null, "startCycle", cycleId.toString());
        this.cycleId = cycleId;
    }

    public Long getCycleId() {
        return cycleId;
    }

    @Override
    public String getRoutingKey() {
        return cycleId.toString();
    }

    @Override
    public String getMessageType() {
        return "startCycle";
    }

    @Override
    public String getContents() {
        return getRoutingKey();
    }

}
