package org.sandcast.naga.message.management;

public class ForwardRemove {

    private final String path;

    public ForwardRemove(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

}
