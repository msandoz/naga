package org.sandcast.naga.message.management;

public class ForwardAdd {

    private final String path;

    public ForwardAdd(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

}
