package org.sandcast.naga.message.management;

public class MessageForward {

    private final Object containedMessage;

    public MessageForward(Object containedMessage) {
        this.containedMessage = containedMessage;
    }

    public Object getContainedMessage() {
        return containedMessage;
    }

}
