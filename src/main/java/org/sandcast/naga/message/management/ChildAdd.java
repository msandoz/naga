package org.sandcast.naga.message.management;

import org.sandcast.naga.dto.PersistedActorDTO;

public class ChildAdd {

    private final PersistedActorDTO dto;

    public ChildAdd(PersistedActorDTO dto) {
        this.dto = dto;
    }

    public PersistedActorDTO getDto() {
        return dto;
    }

}
