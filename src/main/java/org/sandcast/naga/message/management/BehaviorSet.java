package org.sandcast.naga.message.management;

import org.sandcast.naga.behavior.PredicatedFunction;

public class BehaviorSet {

    private final PredicatedFunction behavior;

    public BehaviorSet(PredicatedFunction pf) {
        this.behavior = pf;
    }

    public PredicatedFunction getBehavior() {
        return behavior;
    }

}
