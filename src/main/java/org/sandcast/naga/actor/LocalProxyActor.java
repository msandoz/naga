package org.sandcast.naga.actor;

import akka.actor.AbstractLoggingActor;
import akka.japi.pf.ReceiveBuilder;
import org.sandcast.naga.message.MuStreamMessage;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

public class LocalProxyActor extends AbstractLoggingActor {

    private final PartialFunction<Object, BoxedUnit> behavior;

    public LocalProxyActor(String proxyPath) {
        behavior = ReceiveBuilder
                .match(MuStreamMessage.class, x -> context().actorSelection(proxyPath).forward(x, context()))
                .matchAny(this::unhandled)
                .build();
        receive(behavior);
    }

}
