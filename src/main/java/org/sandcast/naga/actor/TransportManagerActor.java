package org.sandcast.naga.actor;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import org.sandcast.naga.message.distributed.container.AddTransport;
import org.sandcast.naga.message.distributed.container.RegisterRemotableActor;
import org.sandcast.naga.message.distributed.notification.ActorCreatedRemotely;
import org.sandcast.naga.message.distributed.transport.client.SendContainerHeartbeat;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

public class TransportManagerActor extends AbstractLoggingActor {

    private final PartialFunction<Object, BoxedUnit> behavior;
    private final String containerName;

    public TransportManagerActor(String containerName) {
        this.containerName = containerName;
        behavior = ReceiveBuilder
                .match(AddTransport.class, x -> addTransport(x))
                .match(RegisterRemotableActor.class, x -> registerRemotableActor(x))
                .matchAny(this::unhandled)
                .build();
        receive(behavior);
    }

    public PartialFunction<Object, BoxedUnit> migrateActor = null;

    public void addTransport(AddTransport at) {
        ActorRef transport = context().actorOf(at.getProps(), at.getAlias());
        SendContainerHeartbeat ch = new SendContainerHeartbeat();
        transport.tell(ch, self());
    }
    public void registerRemotableActor(RegisterRemotableActor rra) {
        log().info("TM has been requested to register a remotable actor");
        log().info("Creating a local proxy as a placeholder for the local - is this needed or should i just make a registration map :)");
        context().actorOf(Props.create(LocalProxyActor.class, rra.getActorPath()), "");
        ActorCreatedRemotely acr = new ActorCreatedRemotely();
        self().tell(acr, self());
    }

}
