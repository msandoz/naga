package org.sandcast.naga.actor;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.sandcast.naga.dto.PersistedActorDTO;
import org.sandcast.naga.message.distributed.actor.BecomeProxy;
import org.sandcast.naga.message.distributed.container.CreateRemotableActor;
import org.sandcast.naga.message.distributed.container.CreateRemoteActorProxy;
import org.sandcast.naga.message.distributed.container.DumpServers;
import org.sandcast.naga.message.distributed.container.ExposeContainer;
import org.sandcast.naga.message.distributed.migration.MigrateActor;
import org.sandcast.naga.message.distributed.container.RegisterRemotableActor;
import org.sandcast.naga.message.distributed.migration.ActorReadyToBeMigrated;
import org.sandcast.naga.message.distributed.migration.CreateMigrationActor;
import org.sandcast.naga.message.distributed.migration.MigrationActorReady;
import org.sandcast.naga.message.distributed.notification.ContainerAvailable;
import org.sandcast.naga.message.distributed.notification.ContainerHeartbeat;
import org.sandcast.naga.message.distributed.notification.RemoteActorAvailable;
import org.sandcast.naga.message.distributed.notification.RemoteContainerAvailable;
import org.sandcast.naga.message.distributed.migration.PrepareForMigration;
import org.sandcast.naga.message.distributed.migration.ProxyMessage;
import org.sandcast.naga.message.distributed.migration.SendCreateMigrationActor;
import org.sandcast.naga.message.distributed.migration.SendMigrationActorReady;
import org.sandcast.naga.message.distributed.migration.SendProxyMessage;
import org.sandcast.naga.message.distributed.transport.client.SendContainerHeartbeat;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

public class RabbitMQTransportActor extends AbstractLoggingActor {

    private Channel channel;
    private final String alias;
    private final Map<String, Long> hbmap = new HashMap<>();

    public RabbitMQTransportActor(String alias) {
        this.alias = alias;
        try {
            connect();
            transportSetup();
            ecosystemEvents();
            infoEvents();
            containerRequests();
        } catch (Exception e) {
        }

        PartialFunction<Object, BoxedUnit> behavior = ReceiveBuilder
                //
                .match(MigrateActor.class, x -> migrateActor(x))
                .match(ActorReadyToBeMigrated.class, x -> actorReadyToBeMigrated(x))
                .match(SendCreateMigrationActor.class, x -> sendMessageToRemoteContainer(x.getRemoteAddress(), new CreateMigrationActor(x.getState(), x.getRemoteAddress(), x.getOriginalActorPath())))
                .match(CreateMigrationActor.class, x -> createMigrationActor(x))
                .match(SendMigrationActorReady.class, x -> sendMessageToRemoteContainer(x.getRemoteAddress(), new MigrationActorReady(x.getCreatedPath(), x.getRemoteAddress(), x.getOriginalActorPath())))
                .match(MigrationActorReady.class, x -> migrationActorReady(x))
                .match(SendProxyMessage.class, x -> sendMessageToRemoteContainer(x.getRemoteAddress(), new ProxyMessage(x.getMessage(), x.getTargetActorPath())))
                .match(ProxyMessage.class, x -> forwardProxyMessage(x))
                //
                //
                .match(CreateRemotableActor.class, x -> informEcosystemListeners(x))
                .match(CreateRemoteActorProxy.class, x -> informEcosystemListeners(x))
                .match(ExposeContainer.class, x -> informEcosystemListeners(new ContainerAvailable("", "")))
                .match(RegisterRemotableActor.class, x -> informEcosystemListeners(x))
                .match(ContainerHeartbeat.class, x -> {
                    log().info("Akka heartbeat event received for container " + x.getContainer());
                    registerServer(x.getContainer(), System.currentTimeMillis());
                    dumpServers();
                })
                .match(SendContainerHeartbeat.class, x -> informEcosystemListeners(new ContainerHeartbeat(alias)))
                .match(DumpServers.class, x -> dumpServers())
                .matchAny(this::unhandled)
                .build();
        receive(behavior);
    }

    public void migrateActor(MigrateActor ma) {
        log().info("transport has been requested to ask a local actor to prep for migration");
        getContext().actorSelection(ma.getActorPath()).tell(new PrepareForMigration(ma.getRemoteAddress(), ma.getTransport()), self());
    }

    public void actorReadyToBeMigrated(ActorReadyToBeMigrated ma) {
        log().info("transport has been informed of a migration-ready actor; send external system a message requesting to create an actor for migration use");
        SendCreateMigrationActor crma = new SendCreateMigrationActor(ma.getState(), ma.getRemoteAddress(), ma.getOriginalActorPath());
        log().info("logging send state logged " + ma.getState().getClass());

        self().tell(crma, self());
    }

    public void createMigrationActor(CreateMigrationActor ma) {
        log().info("transport has been requested to create a migration actor as requested by a remote container");
        log().info("logged " + ma.getState().getClass());
        ActorRef actor = getContext().actorOf(Props.create(MuStreamActor.class, (PersistedActorDTO) ma.getState()), ma.getOriginalActorPath().replaceAll("/", "-"));
        SendMigrationActorReady ready = new SendMigrationActorReady(actor.path().toStringWithoutAddress(), ma.getRemoteAddress(), ma.getOriginalActorPath());
        self().tell(ready, self());
    }

    public void migrationActorReady(MigrationActorReady ma) {
        log().info("transport has been told that the migration target is ready. now tell original actor to become a proxy for it.");
        BecomeProxy bp = new BecomeProxy(self().path().toStringWithoutAddress(), ma.getRemoteAddress(), ma.getCreatedPath());
        getContext().actorSelection(ma.getOriginalActorPath()).tell(bp, self());
    }

    public void forwardProxyMessage(ProxyMessage ma) {
        log().info("transport has been told that it should forward a message from remote proxy shell to actual implementation");
        
        getContext().actorSelection(ma.getTargetActorPath()).tell(ma.getMessage(), self());
    }

    public void registerServer(String server, long time) {
        hbmap.put(server, time);
    }

    public void dumpServers() {
        log().info("Servers with last contact:");
        hbmap.forEach((x, y) -> log().error("{} : {}", x, new Date(y)));
    }

    public void connect() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://localhost");
        Connection conn = factory.newConnection();
        channel = conn.createChannel();
    }

    public void transportSetup() throws Exception {
        channel.exchangeDeclare("org.sandcast.naga." + alias + ".info", "topic", true);
        channel.exchangeDeclare("org.sandcast.naga.containers", "topic", true);
        channel.queueDeclare("org.sandcast.naga." + alias + ".process", false, false, false, null);
    }

    public void infoEvents() throws Exception {
        log().info("RABBITMQ going to listen to container info on " + "org.sandcast.naga." + alias + ".info");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind("", "org.sandcast.naga." + alias + ".info", "#"); //listen to everyone (this can be filtered) - ie com.musigma.xxx
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body) throws IOException {
                try {
                    Class cl = Class.forName(properties.getHeaders().get("className").toString());
                    Object o = unmarshal(body, cl);
                    log().info("RABBITMQ  Received a message !!!!!!'" + o.toString() + "'");
                    self().tell(o, self());
                } catch (ClassNotFoundException ex) {
                    log().error("Error figuring out sent message");
                }
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }

    public void ecosystemEvents() throws Exception {
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, "org.sandcast.naga.containers", "#"); //listen to everyone (this can be filtered) - ie com.musigma.xxx
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body) throws IOException {
                try {
                    Class cl = Class.forName(properties.getHeaders().get("className").toString());
                    Object o = unmarshal(body, cl);
                    log().info("RABBITMQ Received a ecosystem event!!!!!!'" + o.toString() + "'");
                    self().tell(o, self());
                } catch (ClassNotFoundException ex) {
                    log().error("Error figuring out sent message");
                }
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }

    public void containerRequests() throws Exception {
        log().error("RABBITMQ going to listen to container requests on " + "org.sandcast.naga." + alias + ".process");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body) throws IOException {
                try {
                    Class cl = Class.forName(properties.getHeaders().get("className").toString());
                    Object o = unmarshal(body, cl);
                    log().info("RABBITMQ Received a container request '" + o.toString() + "'");
                    self().tell(o, self());
                } catch (ClassNotFoundException ex) {
                    log().error("Error figuring out sent message");
                }

                //unmarshall the message and then create a real akka message corresponding and send it.
            }
        };
        channel.basicConsume("org.sandcast.naga." + alias + ".process", true, consumer);
    }

    public void informEcosystemListeners(Object s) {
        log().info("Transport has been requested to inform all ecosystem clients of " + s);
        Map<String, Object> headers = new HashMap<>();
        headers.put("className", s.getClass().getName());
        byte[] bytes = marshal(s);
        try {
            channel.basicPublish("org.sandcast.naga.containers", "", new AMQP.BasicProperties.Builder().headers(headers).build(), bytes);
        } catch (IOException ex) {
            log().error("error informing all ecosystem clients of " + s);
        }
    }

    public void informContainerListeners(Object s) {

        log().info("Transport has been requested to inform all our clients that " + s);
        Map<String, Object> headers = new HashMap<>();
        headers.put("className", s.getClass().getName());
        byte[] bytes = marshal(s);
        try {
            channel.basicPublish("org.sandcast.naga." + alias + ".info", "", new AMQP.BasicProperties.Builder().headers(headers).build(), bytes);
        } catch (IOException ex) {
            log().error("error informing all clients that " + s);
        }
    }

    public void sendMessageToRemoteContainer(String container, Object s) {
        log().info("SENDING MESSAGE " + s.getClass().getSimpleName() + " TO REMOTE CONTAINER " + "org.sandcast.naga." + container + ".process");
        Map<String, Object> headers = new HashMap<>();
        headers.put("className", s.getClass().getName());
        byte[] bytes = marshal(s);
        try {
            channel.basicPublish("", "org.sandcast.naga." + container + ".process", new AMQP.BasicProperties.Builder().headers(headers).build(), bytes);
        } catch (IOException ex) {
            log().error("error informing specific container that " + s);
        }
    }

    public void remoteContainerAvailable(RemoteContainerAvailable rca) {
        log().info("Transport has been notified of a remote container becoming available");
        log().info("Transport will now attempt to create locally a proxy for the remote container");
        context().actorOf(Props.create(RemoteContainerProxyActor.class), rca.getAlias());
    }

    public void remoteActorAvailable(RemoteActorAvailable rca) {
        log().info("Transport has been notified of a remote actor becoming available");
        log().info("Transport will now attempt to ask the remote container to create a proxy to the remote actor under it");
        CreateRemoteActorProxy cp = new CreateRemoteActorProxy(rca.getActorPath(), rca.getContainer());
        context().actorSelection(alias).tell(cp, self());
    }

    public byte[] marshal(Object o) {
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED.WRITE_EMPTY_JSON_ARRAYS);
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(o);
        } catch (JsonProcessingException ex) {
            log().error(ex, "FAILED TO MARSHAL");
            return ("failed " + ex.getLocalizedMessage()).getBytes();
        }
    }

    public <C> C unmarshal(byte[] b, Class<C> c) {
        ObjectMapper mapper = new ObjectMapper().enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        try {
            log().info("UNMARSHALING " + new String(b));

            return mapper.readValue(b, c);
        } catch (Exception ex) {
            log().error(ex, "FAILED TO MARSHAL");
            return null;
        }
    }
}
