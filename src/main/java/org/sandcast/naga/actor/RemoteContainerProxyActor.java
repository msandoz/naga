package org.sandcast.naga.actor;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import org.sandcast.naga.message.MuStreamMessage;
import org.sandcast.naga.message.distributed.container.CreateRemoteActorProxy;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

public class RemoteContainerProxyActor extends AbstractLoggingActor {

    private final PartialFunction<Object, BoxedUnit> behavior;

    public RemoteContainerProxyActor(String proxyPath) {
        behavior = ReceiveBuilder
                .match(CreateRemoteActorProxy.class, x -> createRemoteActorProxy(x))
                .match(MuStreamMessage.class, x -> context().parent().forward(x, context()))
                .matchAny(this::unhandled)
                .build();
        receive(behavior);
    }

    public void createRemoteActorProxy(CreateRemoteActorProxy cp) {
        log().info("Remote Container Proxy has been requested to register a proxy for a remote actor");
        log().info("Creating a remote proxy as a placeholder for the remote actor");
        context().actorOf(Props.create(LocalProxyActor.class, cp.getRemotePath()), cp.getLocalAlias());
        log().info("Now have to tell the rest of the world...");
    }

}
