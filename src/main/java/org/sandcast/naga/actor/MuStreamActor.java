package org.sandcast.naga.actor;

import org.sandcast.naga.dto.PersistedActorDTO;
import akka.actor.AbstractActor;
import akka.actor.ActorContext;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.sandcast.naga.dto.BehaviorDTO;
import org.sandcast.naga.behavior.MessageProducingBehavior;
import org.sandcast.naga.message.management.ForwardAdd;
import org.sandcast.naga.message.management.ForwardRemove;
import org.sandcast.naga.message.management.BehaviorSet;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;
import org.sandcast.naga.behavior.PredicatedFunction;
import org.sandcast.naga.behavior.PredicatedFunctionImpl;
import org.sandcast.naga.message.management.ChildAdd;
import org.sandcast.naga.message.management.CycleSetState;
import org.sandcast.naga.message.management.MessageForward;
import org.sandcast.naga.message.MuStreamMessageImpl;
import org.sandcast.naga.message.distributed.actor.BecomeProxy;
import org.sandcast.naga.message.distributed.migration.ActorReadyToBeMigrated;
import org.sandcast.naga.message.distributed.migration.PrepareForMigration;
import org.sandcast.naga.message.distributed.migration.SendProxyMessage;
import org.sandcast.naga.message.management.ChildRemove;
import org.sandcast.naga.message.management.CycleStart;
import scala.concurrent.duration.FiniteDuration;

public class MuStreamActor extends AbstractActor {

    /*
    think about adding a persistent state object that could create a transfer for mobility. could include state of children.
    consider challenges of mutability and requirement that they are frozen at the time and what that means for liveness etc...
     */
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    private PredicatedFunction delegateBehavior;
    private final Set<ActorSelection> downstreams;
    private boolean isCyclic = false;
    private FiniteDuration cycleDelay;
    PersistedActorDTO actorDTO;

    public final PredicatedFunction<Object, ActorContext, String> createBehavior(BehaviorDTO behavior) {
        if (behavior == null) {
            return new PredicatedFunctionImpl(t -> true, u -> {
                unhandled(u);
                return u;
            });
        } else {
            try {
                Class behaviorClass = Class.forName(behavior.getClassName());
                Constructor behaviorConstructor = behaviorClass.getConstructor(String.class, String.class);
                return (PredicatedFunction<Object, ActorContext, String>) behaviorConstructor.newInstance(behavior.getConstructorArgs());
            } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException ex) {
                log.error(ex, "ERROR IN SETTING BEHAVIOR");
                return null;
            }
        }
    }

    private final PartialFunction<Object, BoxedUnit> normalBehavior() {
        return ReceiveBuilder
                .match(PrepareForMigration.class, x -> prepForMigration(x))
                .match(BecomeProxy.class, x -> becomeProxy(x))
                .match(ChildAdd.class, x -> addChild(x))
                .match(ChildRemove.class, x -> removeChild(x))
                .match(ForwardAdd.class, x -> addForwardTo(x))
                .match(ForwardRemove.class, x -> removeForwardTo(x))
                .match(BehaviorSet.class, x -> setBehavior(x))
                .match(MessageForward.class, x -> forwardFlow(x))
                .match(CycleSetState.class, x -> setCyclic(x))
                .match(CycleStart.class, x -> startCycle(x))
                .match(MuStreamMessageImpl.class, x -> routeRuntimeMessage(x))
                .matchAny(this::unhandled)
                .build();
    }

    private final PartialFunction<Object, BoxedUnit> proxyBehavior(BecomeProxy bp) {
        return ReceiveBuilder
                .match(MuStreamMessageImpl.class, x -> proxyMessageToRemote(bp, x))
                .matchAny(this::unhandled)
                .build();
    }

    public MuStreamActor(PersistedActorDTO dto) {
        this.actorDTO = dto;

        this.downstreams = new HashSet<>(1);

        dto.getChildren().stream().forEach(c -> self().tell(new ChildAdd(c), self()));
        delegateBehavior = createBehavior(dto.getBehavior());
        dto.getForwards().stream().forEach(c -> self().tell(new ForwardAdd(c), self()));

        receive(normalBehavior());
        if (dto.isIsCyclic()) {
            self().tell(new CycleSetState(dto.isIsCyclic(), new FiniteDuration(dto.getDuration(), TimeUnit.valueOf(dto.getDurationUnit()))), self());
        }
        log.info("MuStreamActor created :: " + self().path());
    }

    private void proxyMessageToRemote(BecomeProxy bp, MuStreamMessageImpl s) {
        SendProxyMessage pm = new SendProxyMessage(s, bp.getRemoteAddress(), bp.getDestinationPath());
        getContext().actorSelection(bp.getTransport()).forward(pm, getContext());
    }

    private void prepForMigration(PrepareForMigration ma) {
        log.info("MuStreamActor going to prep for migration!!!! :: " + self().path());
        ActorReadyToBeMigrated ready = new ActorReadyToBeMigrated(actorDTO, ma.getTransport(), ma.getRemoteAddress(), self().path().toStringWithoutAddress());
        sender().tell(ready, self());
    }

    private void becomeProxy(BecomeProxy ma) {
        log.info("MuStreamActor preparing to become a proxy for a remote actor!!!! :: " + self().path());
        getContext().become(proxyBehavior(ma));
    }

    private void routeRuntimeMessage(MuStreamMessageImpl s) {
        log.info("MuStreamActor routing a runtime message " + s + self().path());
        if (delegateBehavior.test(s)) {
            Object o = delegateBehavior.apply(s, getContext()); //this has to be much better thought through...
            if (delegateBehavior instanceof MessageProducingBehavior) {
                forward(new MuStreamMessageImpl(o.toString(), "???", s.getRoutingKey()));
            }
        }
    }

    private void startCycle(CycleStart s) {
        Object o = delegateBehavior.apply(s, getContext());
        if (isCyclic) {
            if (cycleDelay == null) {
                self().tell(new CycleStart(s.getCycleId() + 1), self());
            } else {
                getContext().system().scheduler().scheduleOnce(cycleDelay,
                        self(),
                        new CycleStart(s.getCycleId() + 1),
                        getContext().dispatcher(),
                        self());
            }
        }
    }

    private void setBehavior(BehaviorSet s) {
        log.info("removing a behavior");
        delegateBehavior = s.getBehavior();
    }

    private void addChild(ChildAdd s) {
        log.info("adding child " + s.getDto().getName());
        getContext().actorOf(Props.create(MuStreamActor.class, s.getDto()), s.getDto().getName());
    }

    private void removeChild(ChildRemove s) {
        log.info("removing a child");
        ActorRef child = getContext().getChild(s.getName());
        getContext().stop(child);
    }

    private void removeForwardTo(ForwardRemove s) {  
        log.info("removing a forward");
        downstreams.remove(getContext().actorSelection(s.getPath()));
    }

    private void addForwardTo(ForwardAdd s) {
        log.info("adding a forward " + s.getPath());
        downstreams.add(getContext().actorSelection(s.getPath()));
    }

    protected void forward(Object s) {

        downstreams.stream().forEach(ar -> {
            log.info("forwarding to " + ar.toString());
            ar.tell(s, self());
        });
    }

    protected void forward(Object s, ActorRef sender) {
        downstreams.stream().forEach(ar -> ar.tell(s, sender));
    }

    private void setCyclic(CycleSetState s) {
        this.isCyclic = s.isCyclic();
        this.cycleDelay = s.getDuration();
        if (s.isCyclic()) {
            self().tell(new CycleStart(0L), self());
        }
    }

    private void forwardFlow(MessageForward s) {
        this.forward(s.getContainedMessage());
    }

}
