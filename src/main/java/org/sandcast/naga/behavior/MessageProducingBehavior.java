package org.sandcast.naga.behavior;

import org.sandcast.naga.message.MuStreamMessageImpl;

public interface MessageProducingBehavior {

    MuStreamMessageImpl newMessage(String routingKey, String messageType, String contents);

}
