package org.sandcast.naga.behavior;

import akka.actor.ActorContext;
import java.util.function.Function;
import java.util.function.Predicate;

public class PredicatedFunctionImpl<T, U> implements PredicatedFunction<T, ActorContext, U> {

    private final Predicate<T> p;
    private final Function<T, U> c;

    public PredicatedFunctionImpl(Predicate p, Function c) {
        this.p = p;
        this.c = c;
    }

    @Override
    public boolean test(T t) {
        return p.test(t);
    }

    @Override
    public U apply(T t, ActorContext context) {
        return c.apply(t);
    }

}
