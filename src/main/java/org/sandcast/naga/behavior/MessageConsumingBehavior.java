package org.sandcast.naga.behavior;

import akka.actor.ActorContext;
import org.sandcast.naga.message.MuStreamMessageImpl;

public interface MessageConsumingBehavior extends PredicatedFunction<MuStreamMessageImpl, ActorContext, String> {

}
