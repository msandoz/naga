package org.sandcast.naga.behavior;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public interface PredicatedFunction<T, U, V> extends Predicate<T>, BiFunction<T, U, V> {

}
