package org.sandcast.naga.behavior;

import akka.actor.ActorContext;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.sandcast.naga.message.MuStreamMessageImpl;

public class GroovyBehavior implements MessageProducingBehavior, MessageConsumingBehavior {

    private final Script script;
    private final Binding binding;
    private final GroovyShell shell;
    private final String routingKey;

    public GroovyBehavior(String routingKey, String text) {
        this.binding = new Binding();
        this.shell = new GroovyShell(binding);
        script = shell.parse(text);
        this.routingKey = routingKey;
    }

    @Override
    public String apply(MuStreamMessageImpl msg, ActorContext context) {
        script.setProperty("self", context.self());
        script.setProperty("message", msg);
        script.setProperty("context", context);
        String result = script.run().toString();
        context.system().log().warning("result was {} : {}", context.self(), result);
        return result;
    }

    @Override
    public boolean test(MuStreamMessageImpl t) {
//                System.out.println("TEST YIELDS " + routingKey + ":match against:" + t.getRoutingKey());

        return routingKey == null || routingKey.isEmpty() || routingKey.equals(t.getRoutingKey());
    }

    @Override
    public String toString() {
        return "groovy:" + routingKey;
    }

    @Override
    public MuStreamMessageImpl newMessage(String routingKey, String messageType, String contents) {
        return new MuStreamMessageImpl(contents, messageType, routingKey);
    }

}
