package org.sandcast.naga.behavior;

import java.util.function.Predicate;
import org.sandcast.naga.message.MuStreamMessageImpl;

public class Predicates {

    private Predicates() {
    }

    public static Predicate classIs(Class c) {
        return p -> p.getClass().equals(c);
    }

    public static Predicate valueIs(Object v) {
        return p -> p.equals(v);
    }

    public static Predicate<MuStreamMessageImpl> routingKeyIs(String key) {
        return p -> p.getRoutingKey().equals(key);
    }
}
